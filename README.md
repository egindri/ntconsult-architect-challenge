Autor: Eduardo Gindri Lopes

Contato: edgindri@gmail.com


# Teste Arquiteto de Sofware

## System Design

Para projetar, fique a vontade para utilizar a ferramenta que mais te agrada. No dia da apresentação,
para facilitar a discussão, queremos ver:

- Um diagrama de alto nível do projeto
- Modelagem dos dados
- Modelagem da comunicação entre os serviços (HTTP, Mensagens ou Eventos)

## Tarefa

Você foi contratado por uma empresa de mídia social em rápido crescimento para projetar a
arquitetura de um novo sistema de publicação de conteúdo. O sistema deve permitir que milhões
de usuários publiquem texto, fotos e vídeos, interajam com o conteúdo de outros usuários e
recebam notificações em tempo real sobre atividades relevantes. Além disso, o sistema deve incluir
um Single Sign-On (SSO) para permitir que os usuários façam login usando suas credenciais de uma
fonte de identidade confiável.

Seu objetivo é projetar uma arquitetura de sistema escalável, robusta e de alta disponibilidade que
possa lidar com o crescimento esperado no número de usuários e na atividade do sistema, além de
incorporar funcionalidades de SSO para facilitar a autenticação dos usuários.

## Requisitos

1. Escalabilidade

    a. Proponha uma arquitetura escalável que possa lidar com um grande volume de
       usuários e conteúdo.
    
    b. Descreva como o sistema pode ser dimensionado horizontalmente para suportar o
       aumento na carga de trabalho.

2. Disponibilidade e Confiabilidade

    a. Projete o sistema para garantir alta disponibilidade e confiabilidade, minimizando o
       tempo de inatividade e a perda de dados.

    b. Identifique potenciais pontos únicos de falha e proporcione estratégias para mitigar
       esses riscos.

3. Armazenamento de Dados
    
    a. Escolha e justifique a escolha de um banco de dados e sistema de armazenamento
       de dados adequados para o sistema.
    
    b. Desenhe o esquema de banco de dados e explique como ele suportará os requisitos
       de consulta e escrita do sistema.

4. Processamento de Conteúdo
    
    a. Projete o sistema para lidar com diferentes tipos de conteúdo, incluindo texto, fotos
       e vídeos.

    b. Descreva como o sistema processará e armazenará eficientemente cada tipo de
       conteúdo.

5. Notificações em Tempo Real
    
    a. Proponha uma solução para notificações em tempo real que permita aos usuários
       receber atualizações instantâneas sobre atividades relevantes.
    
    b. Descreva a arquitetura de mensageria ou pub/sub que suportará esse recurso.

6. Single Sign-On (SSO)
    
    a. Integre um sistema de Single Sign-On (SSO) ao design do sistema para permitir que
       os usuários façam login usando suas credenciais de uma fonte de identidade
       confiável.
    
    b. Descreva como o SSO será implementado e como ele interagirá com o restante do
       sistema.

7. Segurança
    
    a. Garanta que o sistema seja seguro e protegido contra ameaças comuns, como
       ataques de injeção de código, ataques DDoS e vazamentos de dados.
    
    b. Descreva as medidas de segurança que serão implementadas em cada camada do
       sistema.

8. Monitoramento e Diagnóstico
    
    a. Desenhe uma estratégia de monitoramento e diagnóstico que permita aos
       administradores do sistema rastrear o desempenho, identificar problemas e tomar
       medidas corretivas.
    
    b. Identifique as métricas chave a serem monitoradas e as ferramentas que serão
       usadas para coletar e analisar dados de monitoramento.

## Avaliação

Seu design de sistema será avaliado com base nos seguintes critérios:

- Escalabilidade, disponibilidade e confiabilidade da arquitetura proposta.
- Eficiência e adequação das escolhas de tecnologia e armazenamento de dados.
- Clareza e abrangência da documentação fornecida.
- Implementação eficaz de funcionalidades de SSO.
- Segurança e proteção contra ameaças de segurança.
- Capacidade de demonstrar uma compreensão sólida dos princípios de arquitetura de
    sistemas e design.
- Confiança na apresentação da solução, bem como a qualidade da documentação de arquitetura. Justificando os porquês de decisões tomadas. 

## Nota

- Esteja preparado para explicar e defender suas decisões de design durante uma possível
    entrevista de acompanhamento.
- Certifique-se de incluir seu nome e informações de contato no documento enviado.
- Se precisar de esclarecimentos adicionais sobre os requisitos da prova, sinta-se à vontade
    para entrar em contato com o recrutador responsável antes de iniciar.


